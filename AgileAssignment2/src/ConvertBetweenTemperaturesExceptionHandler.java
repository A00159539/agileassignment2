
public class ConvertBetweenTemperaturesExceptionHandler extends Exception{
	
	String message;
	
	public ConvertBetweenTemperaturesExceptionHandler(String errMessage){
		message = errMessage;
	}
	
	public String getMessage() {
		return message;
	}

}
