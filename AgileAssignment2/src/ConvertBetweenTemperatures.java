
public class ConvertBetweenTemperatures {
	
	double convertFarenheittoCelsius(double farenheit) 
			throws ConvertBetweenTemperaturesExceptionHandler
	{
		// This method converts Farenheit values to Celsius values. 
		// In the range Absolute Zero to 212 degrees Farenheit.
		// On successful conversion, the Celsius amount is returned.

		if( (farenheit < -459.67) )
		{
			throw new ConvertBetweenTemperaturesExceptionHandler("Farenheit value is below Absolute Zero");
		}
		
		else if( (farenheit > 212) )
		{
			throw new ConvertBetweenTemperaturesExceptionHandler("Farenheit value is above 212 degrees");
		}
		
		else return ( (farenheit - 32) * (5.0 / 9.0) ) ;
		
	}

	double convertCelsiustoFarenheit(double celsius) 
			throws ConvertBetweenTemperaturesExceptionHandler
	{
		// This method converts Celsius values to Farenheit values. 
		// In the range Absolute Zero to 100 degrees Celsius.
		// On successful conversion, the Farenheit amount is returned.

		if( (celsius < -273.15) )
		{
			throw new ConvertBetweenTemperaturesExceptionHandler("Celsius value is below Absolute Zero");
		}
		
		else if( (celsius > 100) )
		{
			throw new ConvertBetweenTemperaturesExceptionHandler("Celsius value is above 100 degrees");
		}
		
		else return ( (celsius * (9.0 / 5.0)) + 32 ) ;
		
	}
	
}
