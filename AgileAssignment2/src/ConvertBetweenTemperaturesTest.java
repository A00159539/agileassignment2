import junit.framework.TestCase;

import org.junit.Test;


public class ConvertBetweenTemperaturesTest extends TestCase {

	// Test Number: 1 - Farenheit
	// Test Objective: Test below minimum double boundary
	// Input(s): farenheit = -Double.MAX_VALUE - 0.01
	// Expected Output(s): Exception "Farenheit value is below Absolute Zero"
	
	@Test
	public void testconvertFarenheit0001() 
	{
		ConvertBetweenTemperatures testObject = new ConvertBetweenTemperatures();
		
		try{
			testObject.convertFarenheittoCelsius( (-Double.MAX_VALUE - 0.01) );
			fail("Should not reach here ... Exception expected");
		}
		catch (ConvertBetweenTemperaturesExceptionHandler e) {
			assertEquals("Farenheit value is below Absolute Zero", e.getMessage());
		}
	}
	
	// Test Number: 2 - Farenheit
	// Test Objective: Test minimum double boundary
	// Input(s): farenheit = -Double.MAX_VALUE
	// Expected Output(s): Exception "Farenheit value is below Absolute Zero"
	
	@Test
	public void testconvertFarenheit0002() 
	{
		ConvertBetweenTemperatures testObject = new ConvertBetweenTemperatures();
		
		try{
			testObject.convertFarenheittoCelsius( -Double.MAX_VALUE );
			fail("Should not reach here ... Exception expected");
		}
		catch (ConvertBetweenTemperaturesExceptionHandler e) {
			assertEquals("Farenheit value is below Absolute Zero", e.getMessage());
		}
	}
	
	// Test Number: 3 - Farenheit
	// Test Objective: Test above minimum double boundary
	// Input(s): farenheit = -Double.MAX_VALUE + 0.01
	// Expected Output(s): Exception "Farenheit value is below Absolute Zero"
	
	@Test
	public void testconvertFarenheit0003() 
	{
		ConvertBetweenTemperatures testObject = new ConvertBetweenTemperatures();
		
		try{
			testObject.convertFarenheittoCelsius( (-Double.MAX_VALUE + 0.01) );
			fail("Should not reach here ... Exception expected");
		}
		catch (ConvertBetweenTemperaturesExceptionHandler e) {
			assertEquals("Farenheit value is below Absolute Zero", e.getMessage());
		}
	}
	
	// Test Number: 4 - Farenheit
	// Test Objective: Test below Absolute Zero
	// Input(s): farenheit = -459.68
	// Expected Output(s): Exception "Farenheit value is below Absolute Zero"
	
	@Test
	public void testconvertFarenheit0004() 
	{
		ConvertBetweenTemperatures testObject = new ConvertBetweenTemperatures();
		
		try{
			testObject.convertFarenheittoCelsius(-459.68);
			fail("Should not reach here ... Exception expected");
		}
		catch (ConvertBetweenTemperaturesExceptionHandler e) {
			assertEquals("Farenheit value is below Absolute Zero", e.getMessage());
		}
	}
	
	// Test Number: 5 - Farenheit
	// Test Objective: Test on Absolute Zero boundary
	// Input(s): farenheit = -459.67
	// Expected Output(s): -273.15
	
	@Test
	public void testconvertFarenheit0005() 
	{
		ConvertBetweenTemperatures testObject = new ConvertBetweenTemperatures();
		
		try{
			assertEquals(-273.15, testObject.convertFarenheittoCelsius(-459.67), 0.01);
		}
		catch (ConvertBetweenTemperaturesExceptionHandler e) {
			fail("Should not reach here ... no exception expected");
		}
	}
	
	// Test Number: 6 - Farenheit
	// Test Objective: Test above Absolute Zero
	// Input(s): farenheit = -459.66
	// Expected Output(s): -273.14
	
	@Test
	public void testconvertFarenheit0006() 
	{
		ConvertBetweenTemperatures testObject = new ConvertBetweenTemperatures();
		
		try{
			assertEquals(-273.14, testObject.convertFarenheittoCelsius(-459.66), 0.01);
		}
		catch (ConvertBetweenTemperaturesExceptionHandler e) {
			fail("Should not reach here ... no exception expected");
		}
	}
	
	// Test Number: 7 - Farenheit
	// Test Objective: Test below Zero
	// Input(s): farenheit = -0.01
	// Expected Output(s): -17.78
	
	@Test
	public void testconvertFarenheit0007() 
	{
		ConvertBetweenTemperatures testObject = new ConvertBetweenTemperatures();
		
		try{
			assertEquals(-17.78, testObject.convertFarenheittoCelsius(-0.01), 0.01);
		}
		catch (ConvertBetweenTemperaturesExceptionHandler e) {
			fail("Should not reach here ... no exception expected");
		}
	}
	
	// Test Number: 8 - Farenheit
	// Test Objective: Test Zero boundary
	// Input(s): farenheit = 0
	// Expected Output(s): -17.77
	
	@Test
	public void testconvertFarenheit0008() 
	{
		ConvertBetweenTemperatures testObject = new ConvertBetweenTemperatures();
		
		try{
			assertEquals(-17.77, testObject.convertFarenheittoCelsius(0), 0.01);
		}
		catch (ConvertBetweenTemperaturesExceptionHandler e) {
			fail("Should not reach here ... no exception expected");
		}
	}
	
	// Test Number: 9 - Farenheit
	// Test Objective: Test above Zero
	// Input(s): farenheit = 0.01
	// Expected Output(s): -17.77
	
	@Test
	public void testconvertFarenheit0009() 
	{
		ConvertBetweenTemperatures testObject = new ConvertBetweenTemperatures();
		
		try{
			assertEquals(-17.77, testObject.convertFarenheittoCelsius(0.01), 0.01);
		}
		catch (ConvertBetweenTemperaturesExceptionHandler e) {
			fail("Should not reach here ... no exception expected");
		}
	}
	
	// Test Number: 10 - Farenheit
	// Test Objective: Test below max of 212
	// Input(s): farenheit = 211.9
	// Expected Output(s): 99.94
	
	@Test
	public void testconvertFarenheit0010() 
	{
		ConvertBetweenTemperatures testObject = new ConvertBetweenTemperatures();
		
		try{
			assertEquals(99.94, testObject.convertFarenheittoCelsius(211.9), 0.01);
		}
		catch (ConvertBetweenTemperaturesExceptionHandler e) {
			fail("Should not reach here ... no exception expected");
		}
	}
	
	// Test Number: 11 - Farenheit
	// Test Objective: Test max of 212
	// Input(s): farenheit = 212
	// Expected Output(s): 100
	
	@Test
	public void testconvertFarenheit0011() 
	{
		ConvertBetweenTemperatures testObject = new ConvertBetweenTemperatures();
		
		try{
			assertEquals(100, testObject.convertFarenheittoCelsius(212), 0.01);
		}
		catch (ConvertBetweenTemperaturesExceptionHandler e) {
			fail("Should not reach here ... no exception expected");
		}
	}
	
	// Test Number: 12 - Farenheit
	// Test Objective: Test above max of 212
	// Input(s): farenheit = 212.01
	// Expected Output(s): Exception "Farenheit value is above 212 degrees"
	
	@Test
	public void testconvertFarenheit0012() 
	{
		ConvertBetweenTemperatures testObject = new ConvertBetweenTemperatures();
		
		try{
			testObject.convertFarenheittoCelsius(212.01);
			fail("Should not reach here ... Exception expected");
		}
		catch (ConvertBetweenTemperaturesExceptionHandler e) {
			assertEquals("Farenheit value is above 212 degrees", e.getMessage());
		}
	}
	
	// Test Number: 13 - Farenheit
	// Test Objective: Test below maximum double value boundary
	// Input(s): farenheit = Double.MAX_VALUE - 0.01
	// Expected Output(s): Exception "Farenheit value is above 212 degrees"
	
	@Test
	public void testconvertFarenheit0013() 
	{
		ConvertBetweenTemperatures testObject = new ConvertBetweenTemperatures();
		
		try{
			testObject.convertFarenheittoCelsius( (Double.MAX_VALUE - 0.01) );
			fail("Should not reach here ... Exception expected");
		}
		catch (ConvertBetweenTemperaturesExceptionHandler e) {
			assertEquals("Farenheit value is above 212 degrees", e.getMessage());
		}
	}
	
	// Test Number: 14 - Farenheit
	// Test Objective: Test maximum double value boundary
	// Input(s): farenheit = Double.MAX_VALUE
	// Expected Output(s): Exception "Farenheit value is above 212 degrees"
	
	@Test
	public void testconvertFarenheit0014() 
	{
		ConvertBetweenTemperatures testObject = new ConvertBetweenTemperatures();
		
		try{
			testObject.convertFarenheittoCelsius( Double.MAX_VALUE );
			fail("Should not reach here ... Exception expected");
		}
		catch (ConvertBetweenTemperaturesExceptionHandler e) {
			assertEquals("Farenheit value is above 212 degrees", e.getMessage());
		}
	}
	
	// Test Number: 15 - Farenheit
	// Test Objective: Test above maximum double value boundary
	// Input(s): farenheit = Double.MAX_VALUE + 0.01
	// Expected Output(s): Exception "Farenheit value is above 212 degrees"
	
	@Test
	public void testconvertFarenheit0015() 
	{
		ConvertBetweenTemperatures testObject = new ConvertBetweenTemperatures();
		
		try{
			testObject.convertFarenheittoCelsius( (Double.MAX_VALUE + 0.01) );
			fail("Should not reach here ... Exception expected");
		}
		catch (ConvertBetweenTemperaturesExceptionHandler e) {
			assertEquals("Farenheit value is above 212 degrees", e.getMessage());
		}
	}
	
	
	// Test Number: 1 - Celsius
	// Test Objective: Test below minimum double boundary
	// Input(s): celsius = -Double.MAX_VALUE - 0.01
	// Expected Output(s): Exception "Celsius value is below Absolute Zero"
	
	@Test
	public void testconvertCelsius0001() 
	{
		ConvertBetweenTemperatures testObject = new ConvertBetweenTemperatures();
		
		try{
			testObject.convertCelsiustoFarenheit( (-Double.MAX_VALUE - 0.01) );
			fail("Should not reach here ... Exception expected");
		}
		catch (ConvertBetweenTemperaturesExceptionHandler e) {
			assertEquals("Celsius value is below Absolute Zero", e.getMessage());
		}
	}
	
	// Test Number: 2 - Celsius
	// Test Objective: Test minimum double boundary
	// Input(s): celsius = -Double.MAX_VALUE
	// Expected Output(s): Exception "Celsius value is below Absolute Zero"
	
	@Test
	public void testconvertCelsius0002() 
	{
		ConvertBetweenTemperatures testObject = new ConvertBetweenTemperatures();
		
		try{
			testObject.convertCelsiustoFarenheit( -Double.MAX_VALUE );
			fail("Should not reach here ... Exception expected");
		}
		catch (ConvertBetweenTemperaturesExceptionHandler e) {
			assertEquals("Celsius value is below Absolute Zero", e.getMessage());
		}
	}
	
	// Test Number: 3 - Celsius
	// Test Objective: Test above minimum double boundary
	// Input(s): celsius = -Double.MAX_VALUE + 0.01
	// Expected Output(s): Exception "Celsius value is below Absolute Zero"
	
	@Test
	public void testconvertCelsius0003() 
	{
		ConvertBetweenTemperatures testObject = new ConvertBetweenTemperatures();
		
		try{
			testObject.convertCelsiustoFarenheit( (-Double.MAX_VALUE + 0.01) );
			fail("Should not reach here ... Exception expected");
		}
		catch (ConvertBetweenTemperaturesExceptionHandler e) {
			assertEquals("Celsius value is below Absolute Zero", e.getMessage());
		}
	}
	
	// Test Number: 4 - Celsius
	// Test Objective: Test below Absolute Zero
	// Input(s): celsius = -273.16
	// Expected Output(s): Exception "Celsius value is below Absolute Zero"
	
	@Test
	public void testconvertCelsius0004() 
	{
		ConvertBetweenTemperatures testObject = new ConvertBetweenTemperatures();
		
		try{
			testObject.convertCelsiustoFarenheit(-273.16);
			fail("Should not reach here ... Exception expected");
		}
		catch (ConvertBetweenTemperaturesExceptionHandler e) {
			assertEquals("Celsius value is below Absolute Zero", e.getMessage());
		}
	}
	
	// Test Number: 5 - Celsius
	// Test Objective: Test on Absolute Zero boundary
	// Input(s): celsius = -273.15
	// Expected Output(s): -459.67
	
	@Test
	public void testconvertCelsius0005() 
	{
		ConvertBetweenTemperatures testObject = new ConvertBetweenTemperatures();
		
		try{
			assertEquals(-459.67, testObject.convertCelsiustoFarenheit(-273.15), 0.01);
		}
		catch (ConvertBetweenTemperaturesExceptionHandler e) {
			fail("Should not reach here ... no exception expected");
		}
	}
	
	// Test Number: 6 - Celsius
	// Test Objective: Test above Absolute Zero
	// Input(s): celsius = -273.14
	// Expected Output(s): -459.65
	
	@Test
	public void testconvertCelsius0006() 
	{
		ConvertBetweenTemperatures testObject = new ConvertBetweenTemperatures();
		
		try{
			assertEquals(-459.65, testObject.convertCelsiustoFarenheit(-273.14), 0.01);
		}
		catch (ConvertBetweenTemperaturesExceptionHandler e) {
			fail("Should not reach here ... no exception expected");
		}
	}
	
	// Test Number: 7 - Celsius
	// Test Objective: Test below Zero
	// Input(s): celsius = -0.01
	// Expected Output(s): 31.98
	
	@Test
	public void testconvertCelsius0007() 
	{
		ConvertBetweenTemperatures testObject = new ConvertBetweenTemperatures();
		
		try{
			assertEquals(31.98, testObject.convertCelsiustoFarenheit(-0.01), 0.01);
		}
		catch (ConvertBetweenTemperaturesExceptionHandler e) {
			fail("Should not reach here ... no exception expected");
		}
	}
	
	// Test Number: 8 - Celsius
	// Test Objective: Test Zero boundary
	// Input(s): celsius = 0
	// Expected Output(s): 32
	
	@Test
	public void testconvertCelsius0008() 
	{
		ConvertBetweenTemperatures testObject = new ConvertBetweenTemperatures();
		
		try{
			assertEquals(32, testObject.convertCelsiustoFarenheit(0), 0.01);
		}
		catch (ConvertBetweenTemperaturesExceptionHandler e) {
			fail("Should not reach here ... no exception expected");
		}
	}
	
	// Test Number: 9 - Celsius
	// Test Objective: Test above Zero
	// Input(s): celsius = 0.01
	// Expected Output(s): 32.01
	
	@Test
	public void testconvertCelsius0009() 
	{
		ConvertBetweenTemperatures testObject = new ConvertBetweenTemperatures();
		
		try{
			assertEquals(32.01, testObject.convertCelsiustoFarenheit(0.01), 0.01);
		}
		catch (ConvertBetweenTemperaturesExceptionHandler e) {
			fail("Should not reach here ... no exception expected");
		}
	}
	
	// Test Number: 10 - Celsius
	// Test Objective: Test below max of 100
	// Input(s): celsius = 99.99
	// Expected Output(s): 211.98
	
	@Test
	public void testconvertCelsius0010() 
	{
		ConvertBetweenTemperatures testObject = new ConvertBetweenTemperatures();
		
		try{
			assertEquals(211.98, testObject.convertCelsiustoFarenheit(99.99), 0.01);
		}
		catch (ConvertBetweenTemperaturesExceptionHandler e) {
			fail("Should not reach here ... no exception expected");
		}
	}
	
	// Test Number: 11 - Celsius
	// Test Objective: Test max of 100
	// Input(s): celsius = 100
	// Expected Output(s): 212
	
	@Test
	public void testconvertCelsius0011() 
	{
		ConvertBetweenTemperatures testObject = new ConvertBetweenTemperatures();
		
		try{
			assertEquals(212, testObject.convertCelsiustoFarenheit(100), 0.01);
		}
		catch (ConvertBetweenTemperaturesExceptionHandler e) {
			fail("Should not reach here ... no exception expected");
		}
	}
	
	// Test Number: 12 - Celsius
	// Test Objective: Test above max of 100
	// Input(s): celsius = 100.01
	// Expected Output(s): Exception "Celsius value is above 100 degrees"
	
	@Test
	public void testconvertCelsius0012() 
	{
		ConvertBetweenTemperatures testObject = new ConvertBetweenTemperatures();
		
		try{
			testObject.convertCelsiustoFarenheit(212.01);
			fail("Should not reach here ... Exception expected");
		}
		catch (ConvertBetweenTemperaturesExceptionHandler e) {
			assertEquals("Celsius value is above 100 degrees", e.getMessage());
		}
	}
	
	// Test Number: 13 - Celsius
	// Test Objective: Test below maximum double value boundary
	// Input(s): celsius = Double.MAX_VALUE - 0.01
	// Expected Output(s): Exception "Celsius value is above 100 degrees"
	
	@Test
	public void testconvertCelsius0013() 
	{
		ConvertBetweenTemperatures testObject = new ConvertBetweenTemperatures();
		
		try{
			testObject.convertCelsiustoFarenheit( (Double.MAX_VALUE - 0.01) );
			fail("Should not reach here ... Exception expected");
		}
		catch (ConvertBetweenTemperaturesExceptionHandler e) {
			assertEquals("Celsius value is above 100 degrees", e.getMessage());
		}
	}
	
	// Test Number: 14 - Celsius
	// Test Objective: Test maximum double value boundary
	// Input(s): celsius = Double.MAX_VALUE
	// Expected Output(s): Exception "Celsius value is above 100 degrees"
	
	@Test
	public void testconvertCelsius0014() 
	{
		ConvertBetweenTemperatures testObject = new ConvertBetweenTemperatures();
		
		try{
			testObject.convertCelsiustoFarenheit( Double.MAX_VALUE );
			fail("Should not reach here ... Exception expected");
		}
		catch (ConvertBetweenTemperaturesExceptionHandler e) {
			assertEquals("Celsius value is above 100 degrees", e.getMessage());
		}
	}
	
	// Test Number: 15 - Celsius
	// Test Objective: Test above maximum double value boundary
	// Input(s): celsius = Double.MAX_VALUE + 0.01
	// Expected Output(s): Exception "Celsius value is above 100 degrees"
	
	@Test
	public void testconvertCelsius0015() 
	{
		ConvertBetweenTemperatures testObject = new ConvertBetweenTemperatures();
		
		try{
			testObject.convertCelsiustoFarenheit( (Double.MAX_VALUE + 0.01) );
			fail("Should not reach here ... Exception expected");
		}
		catch (ConvertBetweenTemperaturesExceptionHandler e) {
			assertEquals("Celsius value is above 100 degrees", e.getMessage());
		}
	}
	
}
